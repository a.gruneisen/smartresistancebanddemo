package com.august.customtisensortagclient;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

import java.util.ArrayList;

public class ScannedDevicesListItemDialog extends DialogFragment {





    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        final String[] options = {"Right", "Left"};
        final ArrayList<String> thisScannedDevicesArrayList =
                ((MainActivity)getActivity()).getScannedDevicesArrayList();
        final int scannedDevicesListItemDialogChoice = ((MainActivity)getActivity()).getScannedDevicesListItemDialogChoice();


        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Connect " + thisScannedDevicesArrayList.get(scannedDevicesListItemDialogChoice) + " as Right or Left?")
                .setItems(options, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        // If user selects "Left"
                        if (options[which] == options[1]) {
                            if (((MainActivity)getActivity()).leftConnectedDeviceDisplayInfo.getText() == getString(R.string.MAC_NULL)) {
                                ((MainActivity) getActivity()).connectLeft(thisScannedDevicesArrayList.get(scannedDevicesListItemDialogChoice));
                                ((MainActivity) getActivity()).leftConnectedDeviceDisplayInfo.setText
                                        (thisScannedDevicesArrayList.get(scannedDevicesListItemDialogChoice));
                                // Keep track of which index of ScannedDevicesArrayList is in LeftConnectedDeviceDisplay
                                ((MainActivity) getActivity()).trackIsInLeftConnectedDeviceDisplay(scannedDevicesListItemDialogChoice);
                                ((MainActivity)getActivity()).console.append("Connecting "
                                        + thisScannedDevicesArrayList.get(scannedDevicesListItemDialogChoice) + " to Left\n");
                            } else {
                                ((MainActivity) getActivity()).console.append("Device currently connected to left. Please disconnect first.\n");
                            }
                        } else if (options[which] == options[0]) {
                            if (((MainActivity)getActivity()).rightConnectedDeviceDisplayInfo.getText() == getString(R.string.MAC_NULL)) {
                                ((MainActivity)getActivity()).connectRight(thisScannedDevicesArrayList.get(scannedDevicesListItemDialogChoice));
                                ((MainActivity) getActivity()).rightConnectedDeviceDisplayInfo.setText
                                        (thisScannedDevicesArrayList.get(scannedDevicesListItemDialogChoice));
                                // Keep track of which index of ScannedDevicesArrayList is in RightConnectedDeviceDisplay
                                ((MainActivity)getActivity()).trackIsInRightConnectedDeviceDisplay(scannedDevicesListItemDialogChoice);
                                ((MainActivity)getActivity()).console.append("Connecting "
                                        + thisScannedDevicesArrayList.get(scannedDevicesListItemDialogChoice) + " to Right\n");
                            } else {
                                ((MainActivity) getActivity()).console.append("Device currently connected to right. Please disconnect first.\n");
                            }

                        }


                    }
                });
        return builder.create();
    }
}
