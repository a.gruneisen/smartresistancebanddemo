package com.august.customtisensortagclient;

import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

import java.util.UUID;

public class BluetoothLeServiceForLeft extends Service {

    private final static String TAG = BluetoothLeServiceForLeft.class.getSimpleName();

    private BluetoothManager mBluetoothManager;
    private BluetoothAdapter mBluetoothAdapter;
    private String mBluetoothDeviceAddress;
    private BluetoothGatt mBluetoothGatt;
    private int mConnectionState = STATE_DISCONNECTED;

    String motionServiceUuidString = "F000AA80-0451-4000-B000-000000000000";
    String motionConfUuidString = "f000aa82-0451-4000-b000-000000000000";
    UUID CCC = UUID.fromString("00002902-0000-1000-8000-00805f9b34fb");
    UUID motionService = UUID.fromString(motionServiceUuidString);
    UUID motionConf = UUID.fromString(motionConfUuidString);
    BluetoothGattService leftMotionService;
    BluetoothGattCharacteristic leftMotionChar;

    private static final int STATE_DISCONNECTED = 0;
    private static final int STATE_CONNECTING = 1;
    private static final int STATE_CONNECTED = 2;

    public final static String ACTION_GATT_CONNECTED_LEFT =
            "com.example.bluetooth.le.ACTION_GATT_CONNECTED_LEFT";
    public final static String ACTION_GATT_DISCONNECTED_LEFT =
            "com.example.bluetooth.le.ACTION_GATT_DISCONNECTED_LEFT";
    public final static String ACTION_GATT_SERVICES_DISCOVERED_LEFT =
            "com.example.bluetooth.le.ACTION_GATT_SERVICES_DISCOVERED_LEFT";
    public final static String ACTION_DATA_AVAILABLE_LEFT =
            "com.example.bluetooth.le.ACTION_DATA_AVAILABLE_LEFT";
    public final static String EXTRA_DATA_LEFT =
            "com.example.bluetooth.le.EXTRA_DATA_LEFT";
    public final static String DESCRIPTOR_WRITE_LEFT =
            "descriptor_write_left";
    public final static String CHARACTERISTIC_WRITE_LEFT =
            "characteristic_write_left";



    // Various callback methods defined by the BLE API.
    private final BluetoothGattCallback mGattCallback =
            new BluetoothGattCallback() {
                @Override
                public void onConnectionStateChange(BluetoothGatt gatt, int status,
                                                    int newState) {
                    String intentAction;
                    if (newState == BluetoothProfile.STATE_CONNECTED) {
                        intentAction = ACTION_GATT_CONNECTED_LEFT;
                        mConnectionState = STATE_CONNECTED;
                        broadcastUpdate(intentAction);
                        Log.i(TAG, "Connected to GATT server.");
                        Log.i(TAG, "Attempting to start service discovery:" +
                                mBluetoothGatt.discoverServices());

                    } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                        intentAction = ACTION_GATT_DISCONNECTED_LEFT;
                        mConnectionState = STATE_DISCONNECTED;
                        close();
                        Log.i(TAG, "Disconnected from GATT server and closed.");
                        broadcastUpdate(intentAction);
                    }
                }

                @Override
                // New services discovered
                public void onServicesDiscovered(BluetoothGatt gatt, int status) {
                    if (status == BluetoothGatt.GATT_SUCCESS) {
                        broadcastUpdate(ACTION_GATT_SERVICES_DISCOVERED_LEFT);
                        Log.w(TAG, "GATT services discovered.");
                        turnOnSensor(mBluetoothGatt);
                    } else {
                        Log.w(TAG, "onServicesDiscovered received: " + status);
                    }
                }

                @Override
                public void onCharacteristicRead(BluetoothGatt gatt,
                                                 BluetoothGattCharacteristic characteristic, int status) {
                    if (status == BluetoothGatt.GATT_SUCCESS) {
                        broadcastUpdate(ACTION_DATA_AVAILABLE_LEFT, characteristic, status);
                        Log.w(TAG, "GATT data available left");
                    }
                }

                @Override
                public void onCharacteristicWrite (BluetoothGatt gatt,
                                                   BluetoothGattCharacteristic characteristic,
                                                   int status) {
                    if (status == BluetoothGatt.GATT_SUCCESS) {
                        broadcastUpdate(CHARACTERISTIC_WRITE_LEFT);
                        Log.w(TAG, "characteristic write left");
                    }
                }

                @Override
                public void onDescriptorWrite (BluetoothGatt gatt,
                                               BluetoothGattDescriptor descriptor, int status) {
                    if (status == BluetoothGatt.GATT_SUCCESS) {
                        broadcastUpdate(DESCRIPTOR_WRITE_LEFT);
                        Log.w(TAG, "descriptor write left");
                    }
                }

                @Override
                public void onCharacteristicChanged(BluetoothGatt gatt,
                                                    BluetoothGattCharacteristic characteristic) {
                    broadcastUpdate(ACTION_DATA_AVAILABLE_LEFT, characteristic);
                }
            };
    /**
     * Initializes a reference to the local Bluetooth adapter.
     *
     * @return Return true if the initialization is successful.
     */
    public boolean initialize() {
        // For API level 18 and above, get a reference to BluetoothAdapter through
        // BluetoothManager.
        if (mBluetoothManager == null) {
            mBluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
            if (mBluetoothManager == null) {
                Log.e(TAG, "Unable to initialize BluetoothManager.");
                return false;
            }
        }

        mBluetoothAdapter = mBluetoothManager.getAdapter();
        if (mBluetoothAdapter == null) {
            Log.e(TAG, "Unable to obtain a BluetoothAdapter.");
            return false;
        }

        return true;
    }

    private void broadcastUpdate(final String action) {
        final Intent intent = new Intent(action);
        sendBroadcast(intent);
    }


    private void broadcastUpdate(final String action,
                                 final BluetoothGattCharacteristic characteristic, final int status) {
        final Intent intent = new Intent(action);
        intent.putExtra(EXTRA_DATA_LEFT, characteristic.getValue());
        sendBroadcast(intent);
    }

    private void broadcastUpdate(final String action,
                                 final BluetoothGattCharacteristic characteristic) {
        final Intent intent = new Intent(action);
        intent.putExtra(EXTRA_DATA_LEFT, characteristic.getValue());
        sendBroadcast(intent);
    }

        public class LocalBinder extends Binder {
        BluetoothLeServiceForLeft getService() {
            return BluetoothLeServiceForLeft.this;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        // After using a given device, you should make sure that BluetoothGatt.close() is called
        // such that resources are cleaned up properly.  In this particular example, close() is
        // invoked when the UI is disconnected from the Service.
        close();
        return super.onUnbind(intent);
    }

    public void close() {
        if (mBluetoothGatt == null) {
            return;
        }
        mBluetoothGatt.close();
        mBluetoothGatt = null;
    }

    public boolean connect(final String address) {
        if (mBluetoothAdapter == null || address == null) {
            Log.w(TAG, "BluetoothAdapter not initialized or unspecified address.");
            return false;
        }

        // Previously connected device.  Try to reconnect.
        if (mBluetoothDeviceAddress != null && address.equals(mBluetoothDeviceAddress)
                && mBluetoothGatt != null) {
            Log.d(TAG, "Trying to use an existing mBluetoothGatt for connection.");
            if (mBluetoothGatt.connect()) {
                mConnectionState = STATE_CONNECTING;
                return true;
            } else {
                return false;
            }
        }

        final BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(address);
        if (device == null) {
            Log.w(TAG, "Device not found.  Unable to connect.");
            return false;
        }
        // We want to directly connect to the device, so we are setting the autoConnect
        // parameter to false.
        mBluetoothGatt = device.connectGatt(this, false, mGattCallback);
        Log.d(TAG, "Trying to create a new connection.");
        mBluetoothDeviceAddress = address;
        mConnectionState = STATE_CONNECTING;
        return true;
    }

    public boolean disconnect(final String address) {
        if (mBluetoothAdapter == null || address == null) {
            Log.w(TAG, "BluetoothAdapter not initialized or unspecified address.");
            return false;
        }

        final BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(address);
        if (device == null) {
            Log.w(TAG, "Device not found.  Unable to disconnect.");
            return false;
        }
        mBluetoothGatt.disconnect();
        Log.w(TAG, "Bluetooth device disconnected from GATT");
        return true;

    }

    public BluetoothGatt getBluetoothGatt() {
        return mBluetoothGatt;
    }

    private void turnOnSensor(BluetoothGatt bluetoothGatt) {

        leftMotionService = bluetoothGatt.getService(motionService);
        BluetoothGattCharacteristic config = leftMotionService.getCharacteristic(motionConf);

        byte b[] = new byte[] {0x7F,0x00};
        config.setValue(b);
        if (bluetoothGatt.writeCharacteristic(config)) {
            Log.w(TAG, "Sensor turned on");
        }
    }

    private final IBinder mBinder = new LocalBinder();
}
