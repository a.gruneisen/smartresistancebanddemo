package com.august.customtisensortagclient;

import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;
import java.util.UUID;

public class SensorDataDisplay extends AppCompatActivity {

    private static final String TAG = "SensorDataDisplay";
    TextView leftDataDisplay;
    TextView rightDataDisplay;
    BluetoothLeServiceForLeft mBluetoothLeServiceForLeft;
    boolean mBoundLeft = false;
    BluetoothLeServiceForRight mBluetoothLeServiceForRight;
    boolean mBoundRight = false;
    BluetoothGatt bluetoothGattLeft;
    BluetoothGatt bluetoothGattRight;
    Button viewLeftDataButton;
    public boolean viewLeftDataButtonHasBeenClicked = false;
    Button viewRightDataButton;
    public boolean viewRightDataButtonHasBeenClicked = false;
    Button configureLeftSensorButton;
    Button configureRightSensorButton;
    String motionServiceUuidString = "F000AA80-0451-4000-B000-000000000000";
    String motionCharUuidString = "F000AA81-0451-4000-B000-000000000000";
    UUID motionService = UUID.fromString(motionServiceUuidString);
    UUID motionChar = UUID.fromString(motionCharUuidString);
    UUID CCC = UUID.fromString("00002902-0000-1000-8000-00805f9b34fb");
    UUID motionPeriod = UUID.fromString("F000AA83-0451-4000-B000-000000000000");
    BluetoothGattService leftService;
    BluetoothGattCharacteristic leftChar;
    BluetoothGattService rightService;
    BluetoothGattCharacteristic rightChar;
    byte[] leftBytes;
    String leftString;
    public String left = "left";
    public String right = "right";

    // Create Operation Queue
    private Queue<Operation> operations = new LinkedList<>();
    private Operation currentOp;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sensor_data_display);

        Intent intent = getIntent(); // From Main


        // Get Right and Left Services
        Intent intentLeft = new Intent(this, BluetoothLeServiceForLeft.class);
        bindService(intentLeft, mServiceConnectionLeft, Context.BIND_AUTO_CREATE);
        Intent intentRight = new Intent(this, BluetoothLeServiceForRight.class);
        bindService(intentRight, mServiceConnectionRight, Context.BIND_AUTO_CREATE);

        leftDataDisplay = (TextView) findViewById(R.id.LeftDataDisplay);
        leftDataDisplay.setMovementMethod(new ScrollingMovementMethod());
        rightDataDisplay = (TextView) findViewById(R.id.RightDataDisplay);
        rightDataDisplay.setMovementMethod(new ScrollingMovementMethod());


        viewLeftDataButton = (Button) findViewById(R.id.ViewLeftDataButton);
        viewLeftDataButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getBluetoothGattLeft();
                getServiceLeft(motionService);
                getCharLeft(motionChar);
                if (!viewLeftDataButtonHasBeenClicked) {
                    viewLeftDataButtonHasBeenClicked = true;
                }
                enableLeftNotifications();
                setLeftPeriod();
                readCharLeft();
            }
        });

        viewRightDataButton = (Button) findViewById(R.id.ViewRightDataButton);
        viewRightDataButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getBluetoothGattRight();
                getServiceRight(motionService);
                getCharRight(motionChar);
                if (!viewRightDataButtonHasBeenClicked) {
                    viewRightDataButtonHasBeenClicked = true;
                }
                enableRightNotifications();
                setRightPeriod();
                readCharRight();
            }
        });


        configureLeftSensorButton = (Button) findViewById(R.id.ConfigureLeftSensorButton);
        configureLeftSensorButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        configureRightSensorButton = (Button) findViewById(R.id.ConfigureRightSensorButton);
        configureRightSensorButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        
    }



    private void enableLeftNotifications() {
        request(new Operation(2, 1));
    }

    private void enableRightNotifications() {
        request(new Operation(2, 2));
    }

    private void setLeftPeriod() {
        request(new Operation(3, 1));
        Log.w(TAG, "Period tried set left");
    }

    private void setRightPeriod() {
        request(new Operation(3, 2));
        Log.w(TAG, "Period tried set right");
    }

    public synchronized void request (Operation operation) {
        operations.add(operation);
        if (currentOp == null)
            currentOp = operations.poll();
        if (currentOp.mType == 1) {
            if (currentOp.mWhich == 1) {
                bluetoothGattLeft.readCharacteristic(leftChar);
            } else if (currentOp.mWhich == 2) {
                bluetoothGattRight.readCharacteristic(rightChar);
            }
        } else if (currentOp.mType == 2) {
            if (currentOp.mWhich == 1) {
                if(bluetoothGattLeft.setCharacteristicNotification(leftChar , true)) { //Enabled locally
                    BluetoothGattDescriptor config = leftChar.getDescriptor(CCC);
                    config.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
                    bluetoothGattLeft.writeDescriptor(config); //Enabled remotely
                }
            } else if (currentOp.mWhich == 2) {
                if(bluetoothGattRight.setCharacteristicNotification(rightChar , true)) { //Enabled locally
                    BluetoothGattDescriptor config = rightChar.getDescriptor(CCC);
                    config.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
                    bluetoothGattRight.writeDescriptor(config); //Enabled remotely
                }
            }
        }  else if (currentOp.mType == 3) {
            if (currentOp.mWhich == 1) {

                byte b[] = new byte[] {0x0A};
                BluetoothGattCharacteristic config = leftService.getCharacteristic(motionPeriod);
                config.setValue(b);
                if (bluetoothGattLeft.writeCharacteristic(config)) {
                    Log.w(TAG, "Period set left");
                }
            } else if (currentOp.mWhich == 2) {

                byte b[] = new byte[] {0x0A};
                BluetoothGattCharacteristic config = rightService.getCharacteristic(motionPeriod);
                config.setValue(b);
                if (bluetoothGattRight.writeCharacteristic(config)) {
                    Log.w(TAG, "Period set right");
                }
            }
        }
    }

    public synchronized void operationCompleted () {
        currentOp = null;
        while (operations.peek() != null) {
            currentOp = operations.poll();

            if (currentOp.mType == 1) {
                if (currentOp.mWhich == 1) {
                    bluetoothGattLeft.readCharacteristic(leftChar);
                } else if (currentOp.mWhich == 2) {
                    bluetoothGattRight.readCharacteristic(rightChar);
                }
            } else if (currentOp.mType == 2) {
                if (currentOp.mWhich == 1) {
                    if(bluetoothGattLeft.setCharacteristicNotification(leftChar , true)) { //Enabled locally
                        BluetoothGattDescriptor config = leftChar.getDescriptor(CCC);
                        config.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
                        bluetoothGattLeft.writeDescriptor(config); //Enabled remotely
                    }
                } else if (currentOp.mWhich == 2) {
                    if(bluetoothGattRight.setCharacteristicNotification(rightChar , true)) { //Enabled locally
                        BluetoothGattDescriptor config = rightChar.getDescriptor(CCC);
                        config.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
                        bluetoothGattRight.writeDescriptor(config); //Enabled remotely
                    }
                } else if (currentOp.mType == 3) {
                    if (currentOp.mWhich == 1) {

                        byte b[] = new byte[] {0x0A};
                        BluetoothGattCharacteristic config = leftService.getCharacteristic(motionPeriod);
                        config.setValue(b);
                        if (bluetoothGattLeft.writeCharacteristic(config)) {
                            Log.w(TAG, "Period set left");
                        }
                    } else if (currentOp.mWhich == 2) {

                        byte b[] = new byte[] {0x0A};
                        BluetoothGattCharacteristic config = rightService.getCharacteristic(motionPeriod);
                        config.setValue(b);
                        if (bluetoothGattRight.writeCharacteristic(config)) {
                            Log.w(TAG, "Period set right");
                        }
                    }
                }
            }
        }
    }

    public void getBluetoothGattLeft() {
        bluetoothGattLeft = mBluetoothLeServiceForLeft.getBluetoothGatt();
    }

    public void getBluetoothGattRight() {
        bluetoothGattRight = mBluetoothLeServiceForRight.getBluetoothGatt();
    }

    public void getServiceLeft(UUID service) {
        leftService = bluetoothGattLeft.getService(service);
    }

    public void getCharLeft(UUID characteristic) {
        leftChar = leftService.getCharacteristic(characteristic);
    }

    public void readCharLeft() {
        request(new Operation(1, 1));
    }

    public void getServiceRight(UUID service) {
        rightService = bluetoothGattRight.getService(service);
    }

    public void getCharRight(UUID characteristic) {
        rightChar = rightService.getCharacteristic(characteristic);
    }

    public void readCharRight() {
        request(new Operation(1, 2));
    }


    private ServiceConnection mServiceConnectionLeft = new ServiceConnection() {
        // Called when the connection with the service is established
        @Override
        public void onServiceConnected(ComponentName className, IBinder service) {
            // Because we have bound to an explicit
            // service that is running in our own process, we can
            // cast its IBinder to a concrete class and directly access it.
            BluetoothLeServiceForLeft.LocalBinder binder = (BluetoothLeServiceForLeft.LocalBinder) service;
            mBluetoothLeServiceForLeft = binder.getService();
            mBoundLeft = true;
        }


        // Called when the connection with the service disconnects unexpectedly
        @Override
        public void onServiceDisconnected(ComponentName className) {
            Log.e(TAG, "onServiceDisconnected");
            mBoundLeft = false;
        }
    };

    private ServiceConnection mServiceConnectionRight = new ServiceConnection() {
        // Called when the connection with the service is established
        @Override
        public void onServiceConnected(ComponentName className, IBinder service) {
            // Because we have bound to an explicit
            // service that is running in our own process, we can
            // cast its IBinder to a concrete class and directly access it.
            BluetoothLeServiceForRight.LocalBinder binder = (BluetoothLeServiceForRight.LocalBinder) service;
            mBluetoothLeServiceForRight = binder.getService();
            mBoundRight = true;
        }


        // Called when the connection with the service disconnects unexpectedly
        @Override
        public void onServiceDisconnected(ComponentName className) {
            Log.e(TAG, "onServiceDisconnected");
            mBoundRight = false;
        }
    };


    // Handles various events fired by the Service.
// ACTION_GATT_CONNECTED: connected to a GATT server.
// ACTION_GATT_DISCONNECTED: disconnected from a GATT server.
// ACTION_GATT_SERVICES_DISCOVERED: discovered GATT services.
// ACTION_DATA_AVAILABLE: received data from the device. This can be a
// result of read or notification operations.
    private final BroadcastReceiver mGattUpdateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            boolean mConnected;
            if (BluetoothLeServiceForLeft.ACTION_DATA_AVAILABLE_LEFT.equals(action)) {
                byte[] valueLeft = intent.getByteArrayExtra(BluetoothLeServiceForLeft.EXTRA_DATA_LEFT);
                leftDataDisplay.setText("\n" + "Gyroscope: \n" + "\nx: " + Byte.toString(valueLeft[1])
                        + "\ny: " + Byte.toString(valueLeft[3])
                        + "\nz: " + Byte.toString(valueLeft[5]) + "\n"
                        + "\n" + "Accelerometer: \n" + "\nx: " + Byte.toString(valueLeft[7])
                        + "\ny: " + Byte.toString(valueLeft[9])
                        + "\nz: " + Byte.toString(valueLeft[11]) + "\n");
                operationCompleted();
            } else if (BluetoothLeServiceForRight.ACTION_DATA_AVAILABLE_RIGHT.equals(action)) {
                byte[] valueRight = intent.getByteArrayExtra(BluetoothLeServiceForRight.EXTRA_DATA_RIGHT);
                rightDataDisplay.setText("\n" + "Gyroscope: \n" + "\nx: " + Byte.toString(valueRight[1])
                        + "\ny: " + Byte.toString(valueRight[3])
                        + "\nz: " + Byte.toString(valueRight[5]) + "\n"
                        + "\n" + "Accelerometer: \n" + "\nx: " + Byte.toString(valueRight[7])
                        + "\ny: " + Byte.toString(valueRight[9])
                        + "\nz: " + Byte.toString(valueRight[11]) + "\n");
                operationCompleted();
            } else if (BluetoothLeServiceForLeft.DESCRIPTOR_WRITE_LEFT.equals(action)) {
                operationCompleted();
            } else if (BluetoothLeServiceForRight.DESCRIPTOR_WRITE_RIGHT.equals(action)) {
                operationCompleted();
            } else if (BluetoothLeServiceForLeft.CHARACTERISTIC_WRITE_LEFT.equals(action)) {
                operationCompleted();
            } else if (BluetoothLeServiceForRight.CHARACTERISTIC_WRITE_RIGHT.equals(action)) {
                operationCompleted();
            }
        }
    };


    protected void onResume() {
        super.onResume();
        registerReceiver(mGattUpdateReceiver, makeGattUpdateIntentFilterLeft());
        if (mBluetoothLeServiceForLeft != null) {
            final boolean result = mBluetoothLeServiceForLeft.connect("34:B1:F7:D5:06:A7");
            Log.d(TAG, "Connect left request result=" + result);
        }

        registerReceiver(mGattUpdateReceiver, makeGattUpdateIntentFilterRight());
        if (mBluetoothLeServiceForRight != null) {
            final boolean result = mBluetoothLeServiceForRight.connect("BC:6A:29:AB:DD:9D");
            Log.d(TAG, "Connect right request result=" + result);
        }
    }

    private static IntentFilter makeGattUpdateIntentFilterLeft() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(BluetoothLeServiceForLeft.ACTION_GATT_CONNECTED_LEFT);
        intentFilter.addAction(BluetoothLeServiceForLeft.ACTION_GATT_DISCONNECTED_LEFT);
        intentFilter.addAction(BluetoothLeServiceForLeft.ACTION_GATT_SERVICES_DISCOVERED_LEFT);
        intentFilter.addAction(BluetoothLeServiceForLeft.ACTION_DATA_AVAILABLE_LEFT);
        return intentFilter;
    }

    private static IntentFilter makeGattUpdateIntentFilterRight() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(BluetoothLeServiceForRight.ACTION_GATT_CONNECTED_RIGHT);
        intentFilter.addAction(BluetoothLeServiceForRight.ACTION_GATT_DISCONNECTED_RIGHT);
        intentFilter.addAction(BluetoothLeServiceForRight.ACTION_GATT_SERVICES_DISCOVERED_RIGHT);
        intentFilter.addAction(BluetoothLeServiceForRight.ACTION_DATA_AVAILABLE_RIGHT);
        return intentFilter;
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(mGattUpdateReceiver);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbindService(mServiceConnectionLeft);
        mBoundLeft = false;
        unbindService(mServiceConnectionRight);
        mBoundRight = false;
    }

}
