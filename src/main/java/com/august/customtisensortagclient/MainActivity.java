package com.august.customtisensortagclient;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanResult;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.nfc.Tag;
import android.os.AsyncTask;
import android.os.IBinder;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.io.StringBufferInputStream;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private final static String TAG = AppCompatActivity.class.getSimpleName();



    BluetoothManager mBluetoothManager;
    BluetoothAdapter mBluetoothAdapter;
    BluetoothLeScanner mBluetoothScanner;
    TextView console;
    TextView leftConnectedDeviceDisplayInfo;
    TextView rightConnectedDeviceDisplayInfo;
    Button clear_button;
    Button rightSensorDisconnectButton;
    Button leftSensorDisconnectButton;
    Button rightSensorGetInfoButton;
    Button leftSensorGetInfoButton;
    public ArrayList<String> scannedDevicesArrayList = new ArrayList<String>();
    int scannedDevicesArrayListTop = -1;
    int scannedDevicesListItemDialogChoice;
    int isInLeftConnectedDeviceDisplay;
    int isInRightConnectedDeviceDisplay;
    String whichGetInfoDialog;
    private final static int REQUEST_ENABLE_BT = 1;
    private static final int PERMISSION_REQUEST_COARSE_LOCATION = 1;
    ArrayAdapter<String> myArrayAdapter;
    ListView list;
    BluetoothGatt mBluetoothGatt;
    BluetoothDevice device;
    private BluetoothLeServiceForLeft mBluetoothLeServiceForLeft;
    private BluetoothLeServiceForRight mBluetoothLeServiceForRight;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        console = (TextView) findViewById(R.id.Console);
        console.setMovementMethod(new ScrollingMovementMethod());
        leftConnectedDeviceDisplayInfo = (TextView) findViewById(R.id.LeftConnectedDeviceDisplayInfo);
        rightConnectedDeviceDisplayInfo = (TextView) findViewById(R.id.RightConnectedDeviceDisplayInfo);
        registerClearButton();


        Log.i("MainActivity", "Just a test");



        // Functional code

        setupBluetooth();
        startScanning();
        registerScannedDevicesListCallback();
        registerRightSensorDisconnectButton();
        registerLeftSensorDisconnectButton();
        registerRightSensorGetInfoButton();
        registerLeftSensorGetInfoButton();

        // Needed code

        final Intent intent = getIntent();
        Intent gattServiceIntentLeft = new Intent(this, BluetoothLeServiceForLeft.class);
        bindService(gattServiceIntentLeft, mServiceConnectionLeft, BIND_AUTO_CREATE);
        Intent gattServiceIntentRight = new Intent(this, BluetoothLeServiceForRight.class);
        bindService(gattServiceIntentRight, mServiceConnectionRight, BIND_AUTO_CREATE);


        // Test Code


    }

    







    public void connectLeft(String address) {
        mBluetoothLeServiceForLeft.connect(address);

    }

    public void connectRight(String address) {
        mBluetoothLeServiceForRight.connect(address);

    }

    public void disconnectLeft(String address) {
        mBluetoothLeServiceForLeft.disconnect(address);
    }

    public void disconnectRight(String address) {
        mBluetoothLeServiceForRight.disconnect(address);
    }

    public final ServiceConnection mServiceConnectionLeft = new ServiceConnection() {


        @Override
        public void onServiceConnected(ComponentName componentName, IBinder service) {
            mBluetoothLeServiceForLeft = ((BluetoothLeServiceForLeft.LocalBinder) service).getService();

            if (!mBluetoothLeServiceForLeft.initialize()) {
                Log.e("MainActivity", "Unable to initialize Bluetooth");
                finish();
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            mBluetoothLeServiceForLeft = null;
        }
    };

    public final ServiceConnection mServiceConnectionRight = new ServiceConnection() {


        @Override
        public void onServiceConnected(ComponentName componentName, IBinder service) {
            mBluetoothLeServiceForRight = ((BluetoothLeServiceForRight.LocalBinder) service).getService();

            if (!mBluetoothLeServiceForRight.initialize()) {
                Log.e("MainActivity", "Unable to initialize Bluetooth");
                finish();
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            mBluetoothLeServiceForRight = null;
        }
    };

    // Handles various events fired by the Service.
// ACTION_GATT_CONNECTED: connected to a GATT server.
// ACTION_GATT_DISCONNECTED: disconnected from a GATT server.
// ACTION_GATT_SERVICES_DISCOVERED: discovered GATT services.
// ACTION_DATA_AVAILABLE: received data from the device. This can be a
// result of read or notification operations.
    private final BroadcastReceiver mGattUpdateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            boolean mConnected;
            if (BluetoothLeServiceForLeft.ACTION_GATT_CONNECTED_LEFT.equals(action)) {
                mConnected = true;
                console.append("Found left device\nLooking for services.............\n");
                invalidateOptionsMenu();
            } else if (BluetoothLeServiceForRight.ACTION_GATT_CONNECTED_RIGHT.equals(action)) {
                mConnected = true;
                console.append("Found right device\nLooking for services.............\n");
                invalidateOptionsMenu();
            } else if (BluetoothLeServiceForLeft.ACTION_GATT_DISCONNECTED_LEFT.equals(action)) {
                mConnected = false;
                console.append("Disconnected Left!!!\n");
                invalidateOptionsMenu();
            } else if (BluetoothLeServiceForRight.ACTION_GATT_DISCONNECTED_RIGHT.equals(action)) {
                mConnected = false;
                console.append("Disconnected Right!!!\n");
                invalidateOptionsMenu();
            } else if (BluetoothLeServiceForLeft.
                    ACTION_GATT_SERVICES_DISCOVERED_LEFT.equals(action)) {
                // Show all the supported services and characteristics on the
                // user interface.
                console.append("Services discovered for left.\n");
            } else if (BluetoothLeServiceForRight.
                    ACTION_GATT_SERVICES_DISCOVERED_RIGHT.equals(action)) {
                // Show all the supported services and characteristics on the
                // user interface.
                console.append("Services discovered for right.\n");
            } else if (BluetoothLeServiceForLeft.ACTION_DATA_AVAILABLE_LEFT.equals(action)) {
                console.append(intent.getStringExtra(BluetoothLeServiceForLeft.EXTRA_DATA_LEFT));
            } else if (BluetoothLeServiceForRight.ACTION_DATA_AVAILABLE_RIGHT.equals(action)) {
                console.append(intent.getStringExtra(BluetoothLeServiceForRight.EXTRA_DATA_RIGHT));
            }
        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(mGattUpdateReceiver, makeGattUpdateIntentFilterLeft());
        if (mBluetoothLeServiceForLeft != null) {
            final boolean result = mBluetoothLeServiceForLeft.connect("34:B1:F7:D5:06:A7");
            Log.d(TAG, "Connect left request result=" + result);
        }

        registerReceiver(mGattUpdateReceiver, makeGattUpdateIntentFilterRight());
        if (mBluetoothLeServiceForRight != null) {
            final boolean result = mBluetoothLeServiceForRight.connect("BC:6A:29:AB:DD:9D");
            Log.d(TAG, "Connect right request result=" + result);
        }
    }

    private static IntentFilter makeGattUpdateIntentFilterLeft() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(BluetoothLeServiceForLeft.ACTION_GATT_CONNECTED_LEFT);
        intentFilter.addAction(BluetoothLeServiceForLeft.ACTION_GATT_DISCONNECTED_LEFT);
        intentFilter.addAction(BluetoothLeServiceForLeft.ACTION_GATT_SERVICES_DISCOVERED_LEFT);
        intentFilter.addAction(BluetoothLeServiceForLeft.ACTION_DATA_AVAILABLE_LEFT);
        return intentFilter;
    }

    private static IntentFilter makeGattUpdateIntentFilterRight() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(BluetoothLeServiceForRight.ACTION_GATT_CONNECTED_RIGHT);
        intentFilter.addAction(BluetoothLeServiceForRight.ACTION_GATT_DISCONNECTED_RIGHT);
        intentFilter.addAction(BluetoothLeServiceForRight.ACTION_GATT_SERVICES_DISCOVERED_RIGHT);
        intentFilter.addAction(BluetoothLeServiceForRight.ACTION_DATA_AVAILABLE_RIGHT);
        return intentFilter;
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(mGattUpdateReceiver);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbindService(mServiceConnectionLeft);
        mBluetoothLeServiceForLeft = null;
        unbindService(mServiceConnectionRight);
        mBluetoothLeServiceForRight = null;
    }


    public void clearConsole(){
        console.setText("");
        if (myArrayAdapter != null) {
            myArrayAdapter.notifyDataSetChanged();
        }
        scannedDevicesArrayList.clear();
        scannedDevicesArrayListTop = -1;
    }

    private void registerRightSensorDisconnectButton() {
        rightSensorDisconnectButton = (Button) findViewById(R.id.RightSensorDisconnectButton);
        rightSensorDisconnectButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (rightConnectedDeviceDisplayInfo.getText() == getString(R.string.MAC_NULL)) {
                    console.append("No Right Sensor Connected\n");
                } else {
                    console.append("Right Disconnect\n");
                    rightConnectedDeviceDisplayInfo.setText(R.string.MAC_NULL);
                    disconnectRight(scannedDevicesArrayList.get(isInLeftConnectedDeviceDisplay));
                }
            }
        });
    }

    private void registerLeftSensorDisconnectButton() {
        leftSensorDisconnectButton = (Button) findViewById(R.id.LeftSensorDisconnectButton);
        leftSensorDisconnectButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (leftConnectedDeviceDisplayInfo.getText() == getString(R.string.MAC_NULL)) {
                    console.append("No Left Sensor Connected\n");
                } else {
                    console.append("Left Disconnect\n");
                    leftConnectedDeviceDisplayInfo.setText(R.string.MAC_NULL);
                    disconnectLeft(scannedDevicesArrayList.get(isInLeftConnectedDeviceDisplay));
                }
            }
        });
    }

    private void registerRightSensorGetInfoButton() {
        rightSensorGetInfoButton = (Button) findViewById(R.id.RightSensorGetInfoButton);
        rightSensorGetInfoButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                DialogFragment newFragment = new GetInfoDialog();
                if (rightConnectedDeviceDisplayInfo.getText() == getString(R.string.MAC_NULL))
                    console.append("No Right Sensor Connected\n");
                else {
                    console.append("Right Sensor Get Info\n");
                    whichGetInfoDialog = "Right";
                    newFragment.show(getSupportFragmentManager(), "GetInfoDialog");
                }
            }
        });
    }

    private void registerLeftSensorGetInfoButton() {
        leftSensorGetInfoButton = (Button) findViewById(R.id.LeftSensorGetInfoButton);
        leftSensorGetInfoButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                DialogFragment newFragment = new GetInfoDialog();
                if (leftConnectedDeviceDisplayInfo.getText() == getString(R.string.MAC_NULL))
                    console.append("No Left Sensor Connected\n");
                else {
                    console.append("Left Sensor Get Info\n");
                    whichGetInfoDialog = "Left";
                    newFragment.show(getSupportFragmentManager(), "GetInfoDialog");
                }
            }
        });
    }

    private void registerScannedDevicesListCallback() {
        list = (ListView) findViewById(R.id.myListView);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                TextView textView = (TextView) view;
                scannedDevicesListItemDialogChoice = position;
                DialogFragment newFragment = new ScannedDevicesListItemDialog();
                newFragment.show(getSupportFragmentManager(), "ScannedDevicesListItemDialog");

            }
        });
    }

    private void registerClearButton(){
        clear_button = (Button) findViewById(R.id.clear_button);
        clear_button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                clearConsole();
            }
        });
    }

    public void addDeviceToScannedDevicesList() {
        list = (ListView) findViewById(R.id.myListView);
        myArrayAdapter = new ArrayAdapter<String>(this,
                R.layout.my_list_view_items, scannedDevicesArrayList);
        list.setAdapter(myArrayAdapter);
    }

    public void setupBluetooth() {
        // Get BluetoothAdapter
        final BluetoothManager bluetoothManager =
                (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = bluetoothManager.getAdapter();
        // Get BluetoothLeScanner
        mBluetoothScanner = mBluetoothAdapter.getBluetoothLeScanner();
        // Make sure Bluetooth is enabled
        if (mBluetoothAdapter != null && !mBluetoothAdapter.isEnabled()) {
            Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
        }
        // Make sure location is enabled
        if (this.checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            final AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("This app needs location access");
            builder.setMessage("Please grant location access so this app can detect peripherals.");
            builder.setPositiveButton(android.R.string.ok, null);
            builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                            PERMISSION_REQUEST_COARSE_LOCATION);
                }
            });
            builder.show();
        }
    }

    // Device scan callback.
    private ScanCallback leScanCallback = new ScanCallback() {
        @Override
        public void onScanResult(int callbackType, ScanResult result) {
            while (!scannedDevicesArrayList.contains(result.getDevice().getAddress()) && (result.getRssi()
                    > -65)){
                    scannedDevicesArrayList.add(result.getDevice().getAddress());
                    scannedDevicesArrayListTop++;
                    addDeviceToScannedDevicesList();
                    //console.append(Integer.toString(result.getRssi()) + "\n");
            }
        }
    };

    public ArrayList<String> getScannedDevicesArrayList(){
        return scannedDevicesArrayList;
    }

    public int getScannedDevicesListItemDialogChoice() {
        return scannedDevicesListItemDialogChoice;
    }

    public void trackIsInLeftConnectedDeviceDisplay(int scannedDevicesListItemDialogChoice) {
        isInLeftConnectedDeviceDisplay = scannedDevicesListItemDialogChoice;
    }

    public void trackIsInRightConnectedDeviceDisplay(int scannedDevicesListItemDialogChoice) {
        isInRightConnectedDeviceDisplay = scannedDevicesListItemDialogChoice;
    }

    public int getIsInLeftConnectedDeviceDisplay(){
        return isInLeftConnectedDeviceDisplay;
    }

    public int getIsInRightConnectedDeviceDisplay() {
        return isInRightConnectedDeviceDisplay;
    }

    public String getWhichGetInfoDialog() {
        return whichGetInfoDialog;
    }

    public void startScanning() {
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                mBluetoothScanner.startScan(leScanCallback);
            }
        });
    }

    public void stopScanning() {
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                mBluetoothScanner.stopScan(leScanCallback);
            }
        });
    }










    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_COARSE_LOCATION: {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    System.out.println("coarse location permission granted");
                } else {
                    final AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setTitle("Functionality limited");
                    builder.setMessage("Since location access has not been granted, " +
                            "this app will not be able to discover beacons when in the background.");
                    builder.setPositiveButton(android.R.string.ok, null);
                    builder.setOnDismissListener(new DialogInterface.OnDismissListener() {

                        @Override
                        public void onDismiss(DialogInterface dialog) {
                        }

                    });
                    builder.show();
                }
                return;
            }
        }
    }


}