package com.august.customtisensortagclient;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

import java.util.ArrayList;

public class GetInfoDialog extends DialogFragment {


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {





        final String thisWhichGetInfoDialog = ((MainActivity)getActivity()).getWhichGetInfoDialog();
        final ArrayList<String> thisScannedDevicesArrayList =
                ((MainActivity)getActivity()).getScannedDevicesArrayList();
        final int thisIsInLeftConnectedDeviceDisplay = ((MainActivity)getActivity()).getIsInLeftConnectedDeviceDisplay();
        final int thisIsInRightConnectedDeviceDisplay = ((MainActivity)getActivity()).getIsInRightConnectedDeviceDisplay();
        int thisIsInThisConnectedDeviceDisplay = 0;

        if (thisWhichGetInfoDialog == "Left") {
            thisIsInThisConnectedDeviceDisplay = thisIsInLeftConnectedDeviceDisplay;
        } else if (thisWhichGetInfoDialog == "Right")
            thisIsInThisConnectedDeviceDisplay = thisIsInRightConnectedDeviceDisplay;

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(thisWhichGetInfoDialog + " Sensor Info");
        builder.setMessage("MAC Address: " + thisScannedDevicesArrayList.get(thisIsInThisConnectedDeviceDisplay));
        builder.setNeutralButton("View Data", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent myIntent = new Intent(getContext(), SensorDataDisplay.class);
                myIntent.putExtra("key", "Test"); //Optional parameters
                getContext().startActivity(myIntent);
            }
        });
        builder.setNegativeButton("Done", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User cancelled the dialog
            }
        });
        return builder.create();
    }
}
